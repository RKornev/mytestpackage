using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MyTestPackage
{
    public class TestScript : MonoBehaviour
    {
        public int SomeInt { get; set; }

        private int _somePrivateInt;

        /// <summary>
        /// Start is called before the first frame update
        /// </summary>
        public void Start()
        {
            
        }

        /// <summary>
        /// Update is called once per frame
        /// </summary>
        public void Update()
        {
            
        }

        private void DoSomethingPrivate() { }

        private List<string> ReturnSomethingPrivately() { return null; }
    }
}
